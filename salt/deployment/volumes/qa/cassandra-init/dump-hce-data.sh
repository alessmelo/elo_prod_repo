#dump all hce tables
tables=$(echo 'use hce; desc tables' | cqlsh | xargs -n1 echo)
for table in $tables; do
    echo Dumping $table
    echo "copy hce.$table to 'tabledata/$table' WITH NULL = '__null';" | cqlsh
done;
#remove all table files without data
find . -size  0 -delete
#generate init-data.cql

files=$(ls -1 tabledata/)
echo "" > init-data.cql
for file in $files; do
        echo "truncate hce.$file; " >> init-data.cql
        echo "copy hce.$file from '/opt/cassandra-init/tabledata/$file' WITH NULL = '__null';" >> init-data.cql
done;
