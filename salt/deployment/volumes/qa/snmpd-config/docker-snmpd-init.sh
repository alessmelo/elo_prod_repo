cat >>$SNMPD_CONF <<__EOF__
####  DOCKER MONITORING SECTION  ####
# Monitors in this section will create notification when docker containers change start from/to 'running'
#
# Dictionary:
# c - container
# cd - containerDown
# cu - containerUp
# cde - containerDownEvent
# cue - containerUpEvent
__EOF__

for c in ${containers[@]}
do
  name=${docker_config["${c}_serviceName"]-$c}
  downOid=${docker_config["${c}_containerDown_eventOid"]}
  upOid=${docker_config["${c}_containerUp_eventOid"]}
  refresh_rate=${docker_config["${c}_monitor_refresh_rate"]-${docker_config["monitor_refresh_rate"]}}
  cat >>$SNMPD_CONF <<__EOF__

extend c.$c $SNMP_CONFIG/checkDocker.sh $name
notificationEvent cde.$c $downOid
monitor -e cde.$c  -I -r $refresh_rate cd.$c nsExtendResult.\"c.$c\" != 0
notificationEvent cue.$c $upOid
monitor -e cue.$c -I -r $refresh_rate cu.$c nsExtendResult.\"c.$c\" == 0
__EOF__

done

cat >>$SNMPD_CONF <<__EOF__
####  END OF DOCKER MONITORING SECTION  ####


__EOF__