#!/usr/bin/env bash

addr=$1:${2-80}
response=$(curl -f -XGET "http://$addr/healthcheck/health" 2>/dev/null)
rc=$?;
[[ $rc -ne 0 ]] && exit 1;

status=$(echo $response | jq -r '.["status"]' )
echo $status;
[[ "$status" == "UP" ]]
