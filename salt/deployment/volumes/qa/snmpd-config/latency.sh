#!/usr/bin/env bash

host=$1
port=$2
community=$3
oid=$4
thresholdLow=$5
thresholdHigh=$6

result=$(snmpget -v 2c -O qv -c $community $host:$port $enterpriseOid.$oid)
rc=$?
echo $result
[[ $rc -ne 0 ]] && exit 101
[[ $result -ge $thresholdHigh ]] && exit 2
[[ $result -lt $thresholdLow ]]