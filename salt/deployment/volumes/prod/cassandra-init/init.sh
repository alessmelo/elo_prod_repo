#!/bin/bash

echo -n "Waiting for TCP connection to db:9042 ..."

while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/db/9042" 2> /dev/null
do
    echo -n .
    sleep 1
done
echo 'cassandra is up'

cqlsh 10.82.18.141 9042 -u cassandra -p cassandra < /opt/cassandra-init/init.cql
cqlsh 10.82.18.141 9042 -u cassandra -p cassandra < /opt/cassandra-init/init-data.cql
