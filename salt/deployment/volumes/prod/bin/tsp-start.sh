#!/bin/bash

: ${LOG_DIR=${HOME}/logs}
: ${LOGBACK_PATH=${HOME}/logback/logback.xml}
export JAVA_OPTS="${JAVA_OPTS} -verbose:gc -XX:+PrintGCDetails -Dsun.rmi.dgc.client.gcInterval=3600000 -Dsun.rmi.dgc.server.gcInterval=3600000"
export JAVA_OPTS="${JAVA_OPTS} -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${LOG_DIR} -Xms512m -Xmx512m -XX:MetaspaceSize=128M -XX:MaxMetaspaceSize=128M"
export JAVA_OPTS="${JAVA_OPTS} -Duser.home=/opt/app -Dapp=${APP_NAME} -Dlogback.configurationFile=${LOGBACK_PATH} -Xloggc:${LOG_DIR}/${APP_NAME}-gc.log"
export JAVA_OPTS="${JAVA_OPTS} -XX:+UseCompressedOops -Djava.awt.headless=true -server"
export JAVA_OPTS="${JAVA_OPTS} -Dnet.sf.ehcache.skipUpdateCheck=true"

exec java ${JAVA_OPTS} -jar $@
