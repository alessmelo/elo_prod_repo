#!/bin/bash

export SNMP_CONFIG=${SNMP_CONFIG-$HOME/snmpd-config}
export CONF_LOCATION=${CONF_LOCATION-$HOME/.snmp/}
export SNMPD_CONF=${SNMPD_CONF-$CONF_LOCATION/snmpd.conf}
export TRAP_SINK=${TRAP_SINK-localhost}
export TRAP_SINK_COMMUNITY=${TRAP_SINK_COMMUNITY-secret}
export SNMPD_RO_COMMUNITY=${SNMPD_RO_COMMUNITY-secret}

DEFAULT_SNMP_PORT=${DEFAULT_SNMP_PORT-50799}
DEFAULT_HC_PORT=${DEFAULT_HC_PORT-8079}
DEFAULT_REFRESH_RATE=${DEFAULT_REFRESH_RATE-30}

export enterpriseOid="1.3.6.1.4.1.30703"

#Enrollment
export tsp_enrollment_community=tsp-es-app
export tsp_enrollment_snmp_address=tspesapp:50799
export tspEsOid=401
export tsp_enrollment_snmp_root_oid=.$enterpriseOid.$tspEsOid.1.1

#Notification
export tsp_notification_community=tsp-notification-server
export tsp_notification_snmp_address=notificationServer:50799
export notificationServerOid=402
export tsp_notification_snmp_root_oid=.$enterpriseOid.$notificationServerOid.1.1

#Proxy
export tsp_proxy_community=tsp-detokenizer-network-proxy
export tsp_proxy_snmp_address=detokenizerNetworkProxy:50799
export proxyOid=403
export tsp_proxy_snmp_root_oid=.$enterpriseOid.$proxyOid.1.1

#Online detokenizer
export tsp_online_community=tsp-online-detokenizer
export tsp_online_snmp_address=onlineDetokenizer:50799
export onlineDetokenizerOid=404
export tsp_online_snmp_root_oid=.$enterpriseOid.$onlineDetokenizerOid.1.1

#Offline detokenizer
export tsp_offline_community=tsp-offline-detokenizer
export tsp_offline_snmp_address=offlineDetokenizer:50799
export offlineDetokenizerOid=405
export tsp_offline_snmp_root_oid=.$enterpriseOid.$offlineDetokenizerOid.1.1

#KPI configuration
export systems=(tspes notificationServer proxy onlineDetokenizer_iso onlineDetokenizer_aux offlineDetokenizer)

declare -A kpi_config=(
[tspes_community]="tsp-es-app"
[tspes_host]="tspesapp"
[tspes_port]=$DEFAULT_SNMP_PORT
[tspes_er_oid]="$tspEsOid.1.1.99.2.20.0"
[tspes_er_thresholdLow]="10"
[tspes_er_thresholdHigh]="10"
[tspes_perf_oid]="$tspEsOid.1.1.99.2.8.0"
[tspes_latency_thresholdLow]=1500
[tspes_latency_thresholdHigh]=2000
[tspes_erHigh_eventOid]="$tsp_enrollment_snmp_root_oid.999.21.0"
[tspes_erLow_eventOid]="$tsp_enrollment_snmp_root_oid.999.20.0"
[tspes_perfHigh_eventOid]="$tsp_enrollment_snmp_root_oid.999.30.0"
[tspes_perfLow_eventOid]="$tsp_enrollment_snmp_root_oid.999.31.0"

[notificationServer_community]="tsp-notification-server"
[notificationServer_host]="notificationServer"
[notificationServer_port]=$DEFAULT_SNMP_PORT
[notificationServer_er_oid]="$notificationServerOid.1.1.99.2.20.0"
[notificationServer_er_thresholdLow]="10"
[notificationServer_er_thresholdHigh]="10"
[notificationServer_perf_oid]="$notificationServerOid.1.1.99.2.8.0"
[notificationServer_latency_thresholdLow]=1000
[notificationServer_latency_thresholdHigh]=1500
[notificationServer_erHigh_eventOid]="$tsp_notification_snmp_root_oid.999.21.0"
[notificationServer_erLow_eventOid]="$tsp_notification_snmp_root_oid.999.20.0"
[notificationServer_perfHigh_eventOid]="$tsp_notification_snmp_root_oid.999.30.0"
[notificationServer_perfLow_eventOid]="$tsp_notification_snmp_root_oid.999.31.0"

[proxy_community]="tsp-detokenizer-network-proxy"
[proxy_host]="detokenizerNetworkProxy"
[proxy_port]=$DEFAULT_SNMP_PORT
[proxy_er_oid]="$proxyOid.1.1.4.99.2.20.0"
[proxy_er_thresholdLow]="10"
[proxy_er_thresholdHigh]="10"
[proxy_perf_oid]="$proxyOid.1.1.4.99.2.8.0"
[proxy_latency_thresholdLow]=1000
[proxy_latency_thresholdHigh]=1500
[proxy_erHigh_eventOid]="$tsp_proxy_snmp_root_oid.999.21.0"
[proxy_erLow_eventOid]="$tsp_proxy_snmp_root_oid.999.20.0"
[proxy_perfHigh_eventOid]="$tsp_proxy_snmp_root_oid.999.30.0"
[proxy_perfLow_eventOid]="$tsp_proxy_snmp_root_oid.999.31.0"

[onlineDetokenizer_iso_community]="tsp-online-detokenizer"
[onlineDetokenizer_iso_host]="onlineDetokenizer"
[onlineDetokenizer_iso_port]=$DEFAULT_SNMP_PORT
[onlineDetokenizer_iso_er_oid]="$onlineDetokenizerOid.1.1.5.99.2.20.0"
[onlineDetokenizer_iso_er_thresholdLow]="10"
[onlineDetokenizer_iso_er_thresholdHigh]="10"
[onlineDetokenizer_iso_perf_oid]="$onlineDetokenizerOid.1.1.5.99.2.8.0"
[onlineDetokenizer_iso_latency_thresholdLow]=1000
[onlineDetokenizer_iso_latency_thresholdHigh]=1500
[onlineDetokenizer_iso_erHigh_eventOid]="$tsp_online_snmp_root_oid.999.21.0"
[onlineDetokenizer_iso_erLow_eventOid]="$tsp_online_snmp_root_oid.999.20.0"
[onlineDetokenizer_iso_perfHigh_eventOid]="$tsp_online_snmp_root_oid.999.30.0"
[onlineDetokenizer_iso_perfLow_eventOid]="$tsp_online_snmp_root_oid.999.31.0"

[onlineDetokenizer_aux_community]="tsp-online-detokenizer"
[onlineDetokenizer_aux_host]="onlineDetokenizer"
[onlineDetokenizer_aux_port]=$DEFAULT_SNMP_PORT
[onlineDetokenizer_aux_er_oid]="$onlineDetokenizerOid.1.1.1.99.2.20.0"
[onlineDetokenizer_aux_er_thresholdLow]="10"
[onlineDetokenizer_aux_er_thresholdHigh]="10"
[onlineDetokenizer_aux_perf_oid]="$onlineDetokenizerOid.1.1.1.99.2.8.0"
[onlineDetokenizer_aux_latency_thresholdLow]=1000
[onlineDetokenizer_aux_latency_thresholdHigh]=1500
[onlineDetokenizer_aux_erHigh_eventOid]="$tsp_online_snmp_root_oid.999.23.0"
[onlineDetokenizer_aux_erLow_eventOid]="$tsp_online_snmp_root_oid.999.22.0"
[onlineDetokenizer_aux_perfHigh_eventOid]="$tsp_online_snmp_root_oid.999.32.0"
[onlineDetokenizer_aux_perfLow_eventOid]="$tsp_online_snmp_root_oid.999.33.0"

[offlineDetokenizer_community]="tsp-offline-detokenizer"
[offlineDetokenizer_host]="offlineDetokenizer"
[offlineDetokenizer_port]=$DEFAULT_SNMP_PORT
[offlineDetokenizer_er_oid]="$offlineDetokenizerOid.1.1.1.99.2.20.0"
[offlineDetokenizer_er_thresholdLow]="10"
[offlineDetokenizer_er_thresholdHigh]="10"
[offlineDetokenizer_perf_oid]="$offlineDetokenizerOid.1.1.1.99.2.4.0"
[offlineDetokenizer_latency_thresholdLow]=30000
[offlineDetokenizer_latency_thresholdHigh]=60000
[offlineDetokenizer_erHigh_eventOid]="$tsp_offline_snmp_root_oid.999.21.0"
[offlineDetokenizer_erLow_eventOid]="$tsp_offline_snmp_root_oid.999.20.0"
[offlineDetokenizer_perfHigh_eventOid]="$tsp_offline_snmp_root_oid.999.30.0"
[offlineDetokenizer_perfLow_eventOid]="$tsp_offline_snmp_root_oid.999.31.0"

[monitor_refresh_rate]=$DEFAULT_REFRESH_RATE
)

#replace default values with those configured in system property - system property should have the same name as array key be in uppercase
env_vars_perf=(latency_thresholdLow latency_thresholdHigh er_thresholdLow er_thresholdHigh);
for s in ${systems[@]}; do
  for e in ${env_vars_perf[@]}; do
    varName=${s}_${e}
    varNameUc=${varName^^}
    if [[ -n "${!varNameUc:+z}" ]]; then
      kpi_config[${varName}]=${!varNameUc}
    fi
  done
done

export kpi_config

## Health check config
export hc_systems=(tspes notificationServer httpgateway proxy onlineDetokenizer offlineDetokenizer)

declare -A hc_config=(
[tspes_host]="tspesapp"
[tspes_port]=$DEFAULT_HC_PORT
[tspes_healthDown_eventOid]="$tsp_enrollment_snmp_root_oid.999.11.0"
[tspes_healthUp_eventOid]="$tsp_enrollment_snmp_root_oid.999.10.0"

[httpgateway_host]="httpgateway"
[httpgateway_port]=$DEFAULT_HC_PORT
[httpgateway_healthDown_eventOid]="$enterpriseOid.406.1.1.999.11.0"
[httpgateway_healthUp_eventOid]="$enterpriseOid.406.1.1.999.10.0"

[notificationServer_host]="notificationServer"
[notificationServer_port]=$DEFAULT_HC_PORT
[notificationServer_healthDown_eventOid]="$tsp_notification_snmp_root_oid.999.11.0"
[notificationServer_healthUp_eventOid]="$tsp_notification_snmp_root_oid.999.10.0"

[onlineDetokenizer_host]="onlineDetokenizer"
[onlineDetokenizer_port]=$DEFAULT_HC_PORT
[onlineDetokenizer_healthDown_eventOid]="$tsp_online_snmp_root_oid.999.11.0"
[onlineDetokenizer_healthUp_eventOid]="$tsp_online_snmp_root_oid.999.10.0"

[offlineDetokenizer_host]="offlineDetokenizer"
[offlineDetokenizer_port]=$DEFAULT_HC_PORT
[offlineDetokenizer_healthDown_eventOid]="$tsp_offline_snmp_root_oid.999.11.0"
[offlineDetokenizer_healthUp_eventOid]="$tsp_offline_snmp_root_oid.999.10.0"

[proxy_host]="detokenizerNetworkProxy"
[proxy_port]=$DEFAULT_HC_PORT
[proxy_healthDown_eventOid]="$tsp_proxy_snmp_root_oid.999.11.0"
[proxy_healthUp_eventOid]="$tsp_proxy_snmp_root_oid.999.10.0"

[monitor_refresh_rate]=$DEFAULT_REFRESH_RATE
)
export hc_config

## Docker containers config
#export containers=($(docker ps -a --format "{{.Names}}" | sed 's/,.*//'))
export containers=(
  cassandra1
  cassandra2
  cassandra3
  tspes
  notificationServer
  proxy
  httpgateway
  offlineDetokenizer
  onlineDetokenizer
)

declare -A docker_config=(
[cassandra1_serviceName]="cassandra-1"
[cassandra1_containerDown_eventOid]="$enterpriseOid.501.1.1.999.1.0"
[cassandra1_containerUp_eventOid]="$enterpriseOid.501.1.1.999.0.0"

[cassandra2_serviceName]="cassandra-2"
[cassandra2_containerDown_eventOid]="$enterpriseOid.501.1.2.999.1.0"
[cassandra2_containerUp_eventOid]="$enterpriseOid.501.1.2.999.0.0"

[cassandra3_serviceName]="cassandra-3"
[cassandra3_containerDown_eventOid]="$enterpriseOid.501.1.3.999.1.0"
[cassandra3_containerUp_eventOid]="$enterpriseOid.501.1.3.999.0.0"

[tspes_serviceName]="tspesapp"
[tspes_containerDown_eventOid]="$tsp_enrollment_snmp_root_oid.999.1.0"
[tspes_containerUp_eventOid]="$tsp_enrollment_snmp_root_oid.999.0.0"

[notificationServer_containerDown_eventOid]="$tsp_notification_snmp_root_oid.999.1.0"
[notificationServer_containerUp_eventOid]="$tsp_notification_snmp_root_oid.999.0.0"

[proxy_serviceName]="detokenizerNetworkProxy"
[proxy_containerDown_eventOid]="$tsp_proxy_snmp_root_oid.999.1.0"
[proxy_containerUp_eventOid]="$tsp_proxy_snmp_root_oid.999.0.0"

[httpgateway_containerDown_eventOid]="$enterpriseOid.406.1.1.999.1.0"
[httpgateway_containerUp_eventOid]="$enterpriseOid.406.1.1.999.0.0"

[offlineDetokenizer_containerDown_eventOid]="$tsp_offline_snmp_root_oid.999.1.0"
[offlineDetokenizer_containerUp_eventOid]="$tsp_offline_snmp_root_oid.999.0.0"

[onlineDetokenizer_containerDown_eventOid]="$tsp_online_snmp_root_oid.999.1.0"
[onlineDetokenizer_containerUp_eventOid]="$tsp_online_snmp_root_oid.999.0.0"

[monitor_refresh_rate]=$DEFAULT_REFRESH_RATE
)

if [[ -n "${MONITORED_CONTAINERS:+z}" ]]; then
  export containers=( $(echo $MONITORED_CONTAINERS | sed 's/,/ /g'))
fi

for c in ${containers[@]}; do
  nameVar=${c^^}_SERVICE_NAME
  downOidVar=${c^^}_DOWN_OID
  upOidVar=${c^^}_UP_OID
  if [[ -n "${!nameVar:+z}" ]]; then
    docker_config[${c}_serviceName]=${!nameVar}
  fi
  if [[ -n "${!downOidVar:+z}" ]]; then
    docker_config[${c}_containerDown_eventOid]=${!downOidVar}
  fi
  if [[ -n "${!upOidVar:+z}" ]]; then
    docker_config[${c}_containerUp_eventOid]=${!upOidVar}
  fi
done

export docker_config