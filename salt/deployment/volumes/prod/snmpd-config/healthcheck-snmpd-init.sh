cat >>$SNMPD_CONF <<__EOF__
####  HEALTH CHECK SECTION  ####
# Monitors in this section will create notification when application health changes.
# Health is monitored by http://host:port/healthcheck/health endpoint
#
# Dictionary:
#  hc - healthcheck
#  hd - healthDown
#  hu - healthUp
#  hde - healthDownEvent
#  hue - healthUpEvent
__EOF__
for system in ${hc_systems[@]}
do
  host=${hc_config["${system}_host"]}
  port=${hc_config["${system}_port"]}
  downOid=${hc_config["${system}_healthDown_eventOid"]}
  upOid=${hc_config["${system}_healthUp_eventOid"]}
  refresh_rate=${hc_config["${system}_monitor_refresh_rate"]-${hc_config["monitor_refresh_rate"]}}

  cat >>$SNMPD_CONF <<__EOF__

extend hc.$system $SNMP_CONFIG/healthCheck.sh $host $port
notificationEvent hde.$system $downOid
monitor -e hde.$system -I -r $refresh_rate hd.$system nsExtendResult.\"hc.$system\" != 0
notificationEvent hue.$system $upOid
monitor -e hue.$system -I -r $refresh_rate hu.$system nsExtendResult.\"hc.$system\" == 0
__EOF__

done

cat >>$SNMPD_CONF <<__EOF__
####  END OF HEALTH CHECK SECTION  ####


__EOF__