cat >>$SNMPD_CONF <<__EOF__
####  ERROR RATE SECTION  ####
# Monitors in this section will create notification when error rate threashold is exceeded and when error rate goes back to acceptable levels.
#
# Dictionary:
#  er - errorRate
#  erh - errorRateHigh
#  erl - errorRateLow
#  erhe - errorRateHighEvent
#  erle - errorRateLowEvent
__EOF__
for system in ${systems[@]}
do
  host=${kpi_config["${system}_host"]}
  port=${kpi_config["${system}_port"]}
  community=${kpi_config["${system}_community"]}
  erOid=${kpi_config["${system}_er_oid"]}
  erThresholdHigh=${kpi_config["${system}_er_thresholdHigh"]}
  erThresholdLow=${kpi_config["${system}_er_thresholdLow"]}
  erHighOid=${kpi_config["${system}_erHigh_eventOid"]}
  erLowOid=${kpi_config["${system}_erLow_eventOid"]}
  refresh_rate=${kpi_config["${system}_monitor_refresh_rate"]-${kpi_config["monitor_refresh_rate"]}}
  cat >>$SNMPD_CONF <<__EOF__

extend er.$system $SNMP_CONFIG/errorRate.sh $host $port $community $erOid $erThresholdLow $erThresholdHigh
notificationEvent erhe.$system $erHighOid
monitor -e erhe.$system -I -r $refresh_rate erh.$system nsExtendResult.\"er.$system\" == 2
notificationEvent erle.$system $erLowOid
monitor -e erle.$system -I -r $refresh_rate erl.$system nsExtendResult.\"er.$system\" == 0

__EOF__

done

cat >>$SNMPD_CONF <<__EOF__
####  END OF ERROR RATE SECTION  ####


__EOF__


cat >>$SNMPD_CONF <<__EOF__
####  PERFORMANCE SECTION  ####
# Monitors in this section will create notification when performance drops below threshold and when it goes back to normal.
#
# Dictionary:
# perf - performance
# pl - performanceLow
# ph - performanceHigh
# ple - performanceLowEvent
# phe - performanceHighEvent
__EOF__
for system in ${systems[@]}
do
  host=${kpi_config["${system}_host"]}
  port=${kpi_config["${system}_port"]}
  community=${kpi_config["${system}_community"]}
  perfOid=${kpi_config["${system}_perf_oid"]}
  latencyThresholdLow=${kpi_config["${system}_latency_thresholdLow"]}
  latencyThresholdHigh=${kpi_config["${system}_latency_thresholdHigh"]}
  pLowOid=${kpi_config["${system}_perfLow_eventOid"]}
  pHighOid=${kpi_config["${system}_perfHigh_eventOid"]}
  refresh_rate=${kpi_config["${system}_monitor_refresh_rate"]-${kpi_config["monitor_refresh_rate"]}}
  cat >>$SNMPD_CONF <<__EOF__

extend perf.$system $SNMP_CONFIG/latency.sh $host $port $community $perfOid $latencyThresholdLow $latencyThresholdHigh
notificationEvent ple.$system $pLowOid
monitor -e ple.$system -I -r $refresh_rate pl.$system nsExtendResult.\"perf.$system\" == 2
notificationEvent phe.$system $pHighOid
monitor -e phe.$system -I -r $refresh_rate ph.$system nsExtendResult.\"perf.$system\" == 0
__EOF__

done

cat >>$SNMPD_CONF <<__EOF__
####  END OF PERFORMANCE SECTION  ####


__EOF__