#!/bin/bash

id=$(docker ps --filter "status=running" --filter "name=^/$1$" --format "{{.ID}}")
echo $id;
[[ -n "$id" ]]
