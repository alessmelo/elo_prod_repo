#!/usr/bin/env bash

host=$1
port=$2
community=$3
oid=$4
thresholdLow=$5
thresholdHigh=$6
result=$(snmpget -v 2c -O qv -c $community $host:$port $enterpriseOid.$oid |
  sed 's/^"//' | sed 's/"$//' | sed 's/\\"/"/g' | #remove start and end quote and quote escapes added by snmpget
  jq 'map({key:.status, value:(.count // 0)}) | from_entries
    | {failed: (.FAILED_PROCESSING_STATE // 0), success: (.SUCCESS_PROCESSING_STATE // 0 ), total:(.FAILED_PROCESSING_STATE + .SUCCESS_PROCESSING_STATE // 0) }
    | if .failed == 0 then 0
      else (.failed/.total)*1000
      end
    | floor'
)
rc=$?
echo $result
[[ $rc -ne 0 ]] && exit 101
[[ $result -ge $thresholdHigh ]] && exit 2
[[ $result -lt $thresholdLow ]]
