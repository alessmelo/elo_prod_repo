# system configure state

## This module is responsible for:
- configure server:
    - UTC timezone
    - disable tty
    - configure hosts

## Pillar values that can be used:
- `hosts` define map of hosts like
```
  10.10.10.10: serverA
  10.10.10.11: serverB serverB.com
```
