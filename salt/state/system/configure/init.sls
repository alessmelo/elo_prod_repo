# timezone to UTC
Etc/UTC:
  timezone.system:
    - utc: True

# disable tty
/etc/securetty:
  file.managed:
    - contents: ''

# configure hosts
{%- set hosts = salt['pillar.get']('hosts', {}) %}
{%- for ip, names in hosts.items() | sort %}
{{ ip }}-host-entry:
  host.present:
{% if ip is string %}
    - ip: {{ ip }}
{% else %}
    - ip: {{ ip | first }}
{% endif %}
    - names:
    {% for name in names.split(' ') %}
      - {{ name }}
    {% endfor %}
{% endfor %}
