# locale state

## This module is responsible for:
- configure locale on host

## Pillar values that can be used:
- `locale` (default en_US.UTF-8)
