locale:
  locale.present:
    - name: {{ salt['pillar.get']('locale', 'en_US.UTF-8') }}

default_locale:
  locale.system:
    - name: {{ salt['pillar.get']('locale', 'en_US.UTF-8') }}
    - require:
      - locale: locale
