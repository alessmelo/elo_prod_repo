# disable ipv4
net.ipv6.conf.all.disable_ipv6:
  sysctl.present:
    - value: 1

net.ipv6.conf.default.disable_ipv6:
  sysctl.present:
    - value: 1

net.ipv6.conf.lo.disable_ipv6:
  sysctl.present:
    - value: 1

# optimize momory and tcp
vm.max_map_count:
  sysctl.present:
    - value: 1048575

vm.swappiness:
  sysctl.present:
    - value: 1

net.ipv4.tcp_fin_timeout:
  sysctl.present:
    - value: 30

net.ipv4.tcp_tw_reuse:
  sysctl.present:
    - value: 1

net.ipv4.ip_forward:
  sysctl.present:
    - value: 1

{% set config = '/etc/ssh/sshd_config' %}
{% set volume_users = pillar.get('volume_users', { "app": { "uid": 499 }}).keys() %}
# ulimits for app user
ulimits for app user:
  file.append:
    - name: /etc/security/limits.conf
    - text:
    {% for user in volume_users %}
      - {{ user }} - nofile 1048576
      - {{ user }} - nproc 1048576
      - {{ user }} - as unlimited
      - {{ user }} - memlock unlimited
    {% endfor %}
