volumes:
  group.present:
    - gid: 1008
    - addusers:
      - app
app:
  user.present:
    - shell: /bin/bash
    - home: /home/app
    - uid: 499
    - gid: 991
    - groups:
      - volumes
