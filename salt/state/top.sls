base:
  '*':
    - system.configure
    - system.optimize
    - deployment.volumes
    - deployment.enabled
