# docker managment state

## This module is responsible for:
- install docker package
- configure docker service

## Pillar values that can be used:
- `docker_version` (default 1.9.1) define docker version
- `docker_storage_driver` (default overlay) docker storage driver
- `dns_server` define dns servers list
- `extra_socket` add access through http/https like:  tcp://127.0.0.1:2376
- `registry` define list of docker registries
```
registry:
  - auth: "hashhashhash"
    email: "mail@domain.com"
    address: "https://registry.domain.com"
```

## Requirements
- access to package repositories
- minion should have docker role placed into `/etc/salt/grains`
```
roles:
  - docker
```