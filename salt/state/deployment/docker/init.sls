apply-docker-tweaks:
  file.managed:
    - name: /etc/default/docker
    - template: jinja
    - source: salt://deployment/docker/files/docker-default

{% if salt['pillar.get']('registry', None) %}
/root/.dockercfg:
  file.managed:
    - name: /root/.dockercfg
    - template: jinja
    - source: salt://deployment/docker/files/dockercfg

/root/.docker:
  file.directory:
    - user: root
    - group: root

/root/.docker/config.json:
  file.managed:
    - name: /root/.docker/config.json
    - template: jinja
    - source: salt://deployment/docker/files/config.json
{% else %}
remove-/root/.dockercfg:
  file.absent:
    - name: /root/.dockercfg

remove-/root/.docker/config.json:
  file.absent:
    - name: /root/.docker/config.json
{% endif %}

/etc/docker/certs.d/:
  file.directory:
    - user: root
    - group: root

/etc/docker/certs.d/VVCELHELOADM01:5000/:
  file.directory:
    - user: root
    - group: root

/etc/docker/certs.d/VVCELHELOADM01:5000/ca.crt:
  file.managed:
    - name: /etc/docker/certs.d/VVCELHELOADM01:5000/ca.crt
    - template: jinja
    - source: salt://deployment/docker/files/ca.crt
