{% set volume_users = pillar.get('volume_users', { "app": { "uid": 499 }}) %}
{% set default_users = pillar.get('volume_users', { "app": { "uid": 499 }}).keys()[0] %}

{% set volume_group = salt['group.info']('volumes') %}
{% for user in volume_group.get('members',{}) %}
    {% if user not in volume_users %}
del-user-{{ user }}:
  user.absent:
    - name: {{ user }}
    - purge: true
    - force: true
    {%endif%}
{% endfor %}

add_volumes_group:
  group.present:
    - name: volumes
    - system: false

{% for user, params in volume_users.items() %}
add-{{ user }}-on-{{ grains.get('fqdn') }}:
  user.present:
    - name: {{ user }}
    - createhome: false
    - groups: ['volumes']
    {% if params.get('uid', None) != None %}
    - uid: {{ params.get('uid') }}
    {% endif %}
    - system: true
{% endfor %}

{% for name, params in pillar.get('volumes', {}).items() %}
volume-{{ name }}:
  {% if params.get('source') == none %}
    {% if params.get('type', 'directory') == 'directory' %}
  file.directory:
    - dir_mode: {{ params.get('mode', '755') }}
    {% elif params.get('type', 'directory') == 'file' %}
  file.managed:
    - mode: {{ params.get('mode', '644') }}
    {% endif %}
  {% else %}
    {% if params.get('type', 'directory') == 'directory' %}
  file.recurse:
    - dir_mode: {{ params.get('mode', '755') }}
    {% elif params.get('type', 'directory') == 'file' %}
  file.managed:
    - mode: {{ params.get('mode', '644') }}
    {% endif %}
    - source: salt://deployment/volumes/{{ params.get('source') }}
    {% if params.get('clean', false) %}
    - clean: True
    {% endif %}
    {% if params.get('exclude') is defined %}
    - exclude_pat: {{ params.get('exclude') }}
    {% endif %}
  {% endif %}
  {% if params.get('variables') is defined or pillar.get('global_variables') is defined %}
    {% if params.get('variables') or pillar.get('global_variables') %}
    - template: jinja
    - context:
      {% set variables = params.get('variables', {}) %}
      {% set global_variables = pillar.get('global_variables', {}) %}
      {% do global_variables.update(variables) %}
      {% for key, value in global_variables.items() %}
      {{ key }}: {{ value }}
      {% endfor %}
    {% endif %}
  {% else %}
    - template: {{ params.get('template', '') }}
  {% endif %}
    - name: {{ name }}
    - user: {{ params.get('owner', default_users) }}
    - group: {{ params.get('group', 'volumes') }}
    - makedirs: true
{% endfor %}
