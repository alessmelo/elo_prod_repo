# volumes managment state

## This module is responsible for:
- creation defined files or folders
- copy defined files or folders from repository
- will create user or users for volumes with any system uid by default user app with uid 499 will be present

## Pillar values that can be used:
- `name` file or folder name
- `type` (optional parameter) can be directory or file
- `source` (optional parameter) path to file or folder from repository
- `template` (optional parameter) define template for example jinja (default empty)
- `variables` (optional parameter) variables definition used during templating `source` files
- `mode` (optional parameter) file or folder mode (default 755 for folder or 644 for files)
- `owner` (optional parameter) file or folder mode (default app)
- `group` (optional parameter) file or folder mode (default docker)
- `clean` (optional parameter) to clean volumes before copying from source (default false)
- `exclude` (optional parameter) to exclude subdirs from being cleaned f.e. logs you can add following `exclude: 'E@(^logs/*)'`

## Adding volumes users (optional section)
This is optional section that allows you to define user(s) name and id(s) that can be used as volumes owner(s). By default user app with id 499 is created.
- `name` define name of the user that should be created
- `uid`  define uid of the user that should be create

Example:
```
volume_users:
  www-test:
    uid: 499
  tomcat:
    uid: 998
```

```
volumes:
  /tmp/bitbucket.properties:
    variables:
      LOG_LEVEL: MY_LOG_LEVEL_LOCAL
    source: aaa/bitbucket.properties
    type: file
```

there is also possibility to add global variables:
```
global_variables:
  DB_SERVER_IPADDRESS: bazka
  LOG_LEVEL: global
```

## Requirements
- application config repository should be cloned into `/srv/salt/deployment/volumes`
- access to package repositories
- minion should have compose or docker role placed into `/etc/salt/grains`
```
roles:
  - compose
  - docker
```