# deployment enabled state

## This module is responsible for:
- deploy composition file 

There is also possibility to use global variables inside docker-compose file:

## Pillar values that can be used:
- `compose_force_start` (default false) define whether you want to add cron job with `docker-compose up -d`

## Requirements
- compose repository should be cloned into `/srv/salt/deployment/compose`
- minion should have deployment role placed into `/etc/salt/grains`
```
roles:
  - deployment
```