# make sure we have compose
include:
  - deployment.compose

/etc/docker/docker-compose.yml:
  file.managed:
    - name: /etc/docker/docker-compose.yml
    - user: root
    - group: docker
    - makedirs: true
    - source: salt://deployment/compose/{{ grains['id'] }}.yml
    - source: salt://deployment/compose/{{ grains['id'] }}.yaml
  {% if pillar.get('global_variables') is defined %}
    {% if pillar.get('global_variables') %}
    - template: jinja
    - context:
      {% set global_variables = pillar.get('global_variables', {}) %}
      {% for key, value in global_variables.items() %}
      {{ key }}: {{ value }}
      {% endfor %}
    {% endif %}
  {% endif %}

{% if salt['pillar.get']('compose_force_start', false) %}
add-/etc/cron.d/compose:
  file.managed:
    - name: /etc/cron.d/compose
    - user: root
    - group: docker
    - source: salt://deployment/compose/files/compose-cron
    - template: jinja
{% else %}
remove-/etc/cron.d/compose:
  file.absent:
    - name: /etc/cron.d/compose
{% endif %}

Run compose only if docker-compose.yml was changed:
  cmd.wait:
{% if grains['os_family'] == 'RedHat' %}
    - name: source /var/container_data/mvn_vars.conf && /usr/bin/docker-compose -f /etc/docker/docker-compose.yml up -d 2>&1 | /usr/bin/logger -t docker-compose
{% elif grains['os_family'] == 'Debian' %}
    - name: source /var/container_data/mvn_vars.conf && /usr/local/bin/docker-compose -f /etc/docker/docker-compose.yml up -d 2>&1 | /usr/bin/logger -t docker-compose
{% endif %}
    - watch:
      - file: /etc/docker/docker-compose.yml
    - require:
      - file: /etc/docker/docker-compose.yml
