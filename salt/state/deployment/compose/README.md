# docker-compose managment state

## This module is responsible for:
- install docker (by running docker state)
- installing docker-compose package

## Pillar values that can be used:
- `compose_version` (default 1.5.2) define docker-compose version

## Requirements
- compose repository should be cloned into `/srv/salt/deployment/compose`
- access to package repositories
- minion should have compose role placed into `/etc/salt/grains`
```
roles:
  - compose
```